<?php
	$app->get("/search/:person", function($person) use ($app, $db) {
		// prepare array output

		$output = array();
		// query database
		$queries = explode(" ", $person);
		$found = array();

		for($i = 0; $i < count($queries); $i++) {
			$queryResult = $db->person()->where("firstName like '%$queries[$i]%' OR middleName like '%$queries[$i]%' OR lastName like '%$queries[$i]%'");

			foreach ($queryResult as $person) {
				if(!in_array($person["id"], $found)) {
					$found[] = $person["id"];
					$output[] = array(
						"person_id"			=> $person["id"],
						"person_firstName"	=> $person["firstName"],
						"person_middleName"	=> $person["middleName"],
						"person_lastName"	=> $person["lastName"],
						"person_position"	=> $person["position"]
					);
				}
			}
		}
		// format and send output
		ResponseHelper::echoResponse(200, $output);
	});
?>