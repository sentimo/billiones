<?php
	$app->get("/person/:id", function($id) use ($app, $db) {
		// query database
		$queryResult = $db->person()->where("id", $id);

		// prepare array output
		$output = array();
		foreach ($queryResult as $person) {
			$output[] = array(
				"person_id"			=> $person["id"],
				"person_firstName"	=> $person["firstName"],
				"person_middleName"	=> $person["middleName"],
				"person_lastName"	=> $person["lastName"],
				"person_position"	=> $person["position"],
				"person_intro"		=> $person["intro"]
			);
		}
		// format and send output
		ResponseHelper::echoResponse(200, $output);
	});
?>