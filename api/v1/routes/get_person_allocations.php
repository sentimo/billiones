<?php
	$app->get("/get_person_allocations", function() use ($app, $db) {
		// query database
		$queryResult = $db->person_allocation();

		// prepare array output
		$output = array();
		foreach ($queryResult as $person_allocation) {
			$output[] = array(
				"person_allocation_id"				=> $person_allocation["id"],
				"person_allocation_person_id"		=> $person_allocation["person_id"],
				"person_allocation_water"			=> $person_allocation["water"],
				"person_allocation_education"		=> $person_allocation["education"],
				"person_allocation_transportation"	=> $person_allocation["transportation"],
				"person_allocation_infrastructure"	=> $person_allocation["infrastructure"],
				"person_allocation_livelihood"		=> $person_allocation["livelihood"]
			);
		}
		// format and send output
		ResponseHelper::echoResponse(200, $output);
	});
?>