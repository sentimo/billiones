<?php
	// -- load config files
	require_once("vendor/config.php");
	if (file_exists("vendor/config-local.php")) {
		include_once("vendor/config-local.php");
	}
	
	// -- load custom functions
	require_once("vendor/functions.php");

	// -- setup Slim
	require_once("vendor/Slim/Slim.php");
	\Slim\Slim::registerAutoloader();

	$app = new \Slim\Slim();

	// -- setup CORS for cross domain API access
	require_once("vendor/Slim/Middleware/CorsSlim.php");
	$corsOptions = array(
		"origin" => "http://localhost",
		"allowMethods" => array("POST","GET"),
		"allowHeaders" => "Content-Type"
	);
	$app->add(new \CorsSlim\CorsSlim($corsOptions));

	// -- setup NotORM for database access
	require_once("vendor/NotORM.php");

	$dsn = $config["db-adapter"]
		. ":host=" . $config["db-host"]
		. ";dbname=" . $config["db-name"] . ";";
	$pdo = new PDO($dsn, $config["db-user"], $config["db-pass"]);
	$db = new NotORM($pdo);

	// -- setup timezone
	date_default_timezone_set("Asia/Singapore");

	// -- load route files
	include_once("routes/announcements.php");
	include_once("routes/announcement.id.php");
	include_once("routes/persons.php");
	include_once("routes/persons.position.php");
	include_once("routes/person.id.php");
	include_once("routes/person.id.expenses.php");
	include_once("routes/person.id.expenses.summary.php");
	include_once("routes/person.id.donations.php");
	include_once("routes/person.id.donations.summary.php");
	include_once("routes/person.id.salns.php");
	include_once("routes/expenses.php");
	include_once("routes/donations.php");
	include_once("routes/search.person.php");
	include_once("routes/init_person_allocations.php");
	include_once("routes/get_person_allocations.php");

	// -- execute Slim application
	$app->run();
?>