(function() {
	// define a new directive for the global navigation interface
	angular.module("app.directives.topMenu", []).directive("topMenu", function() {
		return {
			restrict: "E",
			templateUrl: "app/directives/topMenu/topMenu.html",
		}
	});
})();