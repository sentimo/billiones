(function() {
	"use strict";

	angular.module("app.states.announcement", [
		"restangular",
		"ui.router"
	])
	.config(function($stateProvider) {
		$stateProvider.state("announcement", {
			url: "/announcements/{announcementId:int}",
			templateUrl: "app/states/announcement/announcement.html",
			controller: "AnnouncementController",
			resolve: {
				$title: function() { return "Announcement"; }
			}
		});
	}).controller("AnnouncementController", function($stateParams, $scope, AnnouncementService) {
		$scope.announcement = AnnouncementService.getAnnouncement($stateParams.announcementId);
	});
}());