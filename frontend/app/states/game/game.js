(function() {
	"use strict";

	angular.module("app.states.game", [
		"restangular",
		"ui.router"
	])
	.config(function($stateProvider) {
		$stateProvider.state("game", {
			url: "/game",
			templateUrl: "app/states/game/game.html",
			controller: "GameController",
			resolve: {
				$title: function() { return "Game"; }
			}
		});
	}).controller("GameController", function($rootScope, $scope, $state, GameService) {
		$scope.game = {};

		$scope.game.water = 0;
		$scope.game.waterPercent = 0;
		$scope.game.education = 0;
		$scope.game.educationPercent = 0;
		$scope.game.transportation = 0;
		$scope.game.transportationPercent = 0;
		$scope.game.infrastructure = 0;
		$scope.game.infrastructurePercent = 0;
		$scope.game.livelihood = 0;
		$scope.game.livelihoodPercent = 0;
		$scope.budget = 1000000;

		$("#gameForm input").keyup(function() {
			if($scope.budget == 0) {
				console.log("hi");
			}
			$scope.budget = Math.round((1000000 - (parseFloat($scope.game.water) + parseFloat($scope.game.education) + parseFloat($scope.game.transportation) + parseFloat($scope.game.infrastructure) + parseFloat($scope.game.livelihood)))*100)/100;
			$scope.game.waterPercent = (Math.round(($scope.game.water / 1000000) * 10000)/100);
			$scope.game.educationPercent = (Math.round(($scope.game.education / 1000000) * 10000)/100);
			$scope.game.transportationPercent = (Math.round(($scope.game.transportation / 1000000) * 10000)/100);
			$scope.game.infrastructurePercent = (Math.round(($scope.game.infrastructure / 1000000) * 10000)/100);
			$scope.game.livelihoodPercent = (Math.round(($scope.game.livelihood / 1000000) * 10000)/100);
			$scope.$apply();
			if($scope.budget == 0) {
				$scope.validForm = 1;
			} else {
				$scope.validForm = 0;
			}
		});

		$scope.submit = function(isValid) {
			if(isValid) {
				var data = {
					water : $scope.game.water,
					education : $scope.game.education,
					transportation : $scope.game.transportation,
					infrastructure : $scope.game.infrastructure,
					livelihood : $scope.game.livelihood
				};
				$rootScope.allocation = GameService.computeAllocation(data);
				$state.go("results");
			} else {
				console.log('Form is not valid');
			}
		}
	});
}());