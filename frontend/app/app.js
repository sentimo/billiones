(function() {
	"use strict";

	// setup app's module dependencies
	var app = angular.module("app", [
		"ui.router",
		"ui.router.title",
		"app.states.home",
		"app.states.profile",
		"app.states.results",
		"app.states.game",
		"app.states.announcement",
		"app.states.compare",
		"app.directives.topMenu",
		"app.directives.expensesGraph",
		"restangular"]);

	// configure app
	app.config(function($urlRouterProvider, RestangularProvider) {

		// redirects any unmatched url
		$urlRouterProvider.otherwise("/");

		// configure restangular		
		RestangularProvider.setBaseUrl("../api/v1");
		RestangularProvider.setDefaultRequestParams('jsonp', {callback: 'JSON_CALLBACK'});
		RestangularProvider.setDefaultHeaders({'Content-Type': 'application/json'});
	});
}());