(function() {
	'use strict';

	angular.module("app").factory("GameService", function($rootScope, $timeout, Restangular, PersonService) {
		return {
			computeAllocation: function(data) {
				var allocation = {
					water : data.water / 10000 + "%",
					education : data.education / 10000 + "%",
					transportation : data.transportation / 10000 + "%",
					infrastructure : data.infrastructure / 10000 + "%",
					livelihood : data.livelihood / 10000 + "%"
				}
				return allocation;
			},
			getPersonAllocations: function() {
				return Restangular.all("get_person_allocations").getList().$object;
			},
			computeCorrelation: function(allocation) {
				var first = 0, second = 0, third = 0;
				var first_person = 0, second_person = 0, third_person = 0;
				var xy, numerator, denominator;
				var self = this;
				var allocations = self.getPersonAllocations();
				var x1 = 1*parseFloat(allocation.water);
				var x2 = 2*parseFloat(allocation.education);
				var x3 = 3*parseFloat(allocation.transportation);
				var x4 = 4*parseFloat(allocation.infrastructure);
				var x5 = 5*parseFloat(allocation.livelihood);

				var xave = (x1+x2+x3+x4+x5)/5;

				var x1n = x1 - xave;
				var x2n = x2 - xave;
				var x3n = x3 - xave;
				var x4n = x4 - xave;
				var x5n = x5 - xave;

				$timeout(function() {
					var num = allocations.length;
					for(var i = 0; i < num; i++) {
						var y1 = 1*parseFloat(allocations[i]["person_allocation_water"]);
						var y2 = 2*parseFloat(allocations[i]["person_allocation_education"]);
						var y3 = 3*parseFloat(allocations[i]["person_allocation_transportation"]);
						var y4 = 4*parseFloat(allocations[i]["person_allocation_infrastructure"]);
						var y5 = 5*parseFloat(allocations[i]["person_allocation_livelihood"]);

						var yave = (y1+y2+y3+y4+y5)/5;

						var y1n = y1 - yave;
						var y2n = y2 - yave;
						var y3n = y3 - yave;
						var y4n = y4 - yave;
						var y5n = y5 - yave;

						var xy1n = x1n * y1n;
						var xy2n = x2n * y2n;
						var xy3n = x3n * y3n;
						var xy4n = x4n * y4n;
						var xy5n = x5n * y5n;

						var xysumn = xy1n + xy2n + xy3n + xy4n + xy5n;

						var x1d = x1n * x1n;
						var x2d = x2n * x2n;
						var x3d = x3n * x3n;
						var x4d = x4n * x4n;
						var x5d = x5n * x5n;

						var y1d = y1n * y1n;
						var y2d = y2n * y2n;
						var y3d = y3n * y3n;
						var y4d = y4n * y4n;
						var y5d = y5n * y5n;

						var xsumd = x1d + x2d + x3d + x4d + x5d;
						var ysumd = y1d + y2d + y3d + y4d + y5d;

						var sqrtxsumd = Math.sqrt(xsumd);
						var sqrtysumd = Math.sqrt(ysumd);

						var xysumd = sqrtxsumd * sqrtysumd;

						var r = xysumn / xysumd;
						var r2 = r * r;

						if(r2 > third) {
							third = r2;
							third_person = i + 1;
							if(r2 > second) {
								third = second;
								third_person = second_person;
								second = r2;
								second_person = i + 1;
								if(r2 > first) {
									second = first;
									second_person = first_person;
									first = r2;
									first_person = i + 1;
								}
							}
						}
					}
					$rootScope.correlation_value_first = (Math.round(first * 10000) / 100) + "%";
					$rootScope.correlation_value_second = (Math.round(second * 10000) / 100) + "%";
					$rootScope.correlation_value_third = (Math.round(third * 10000) / 100) + "%";
					$rootScope.correlation_person_first = PersonService.getPerson(first_person);
					$rootScope.correlation_person_second = PersonService.getPerson(second_person);
					$rootScope.correlation_person_third = PersonService.getPerson(third_person);
				}, 500);
			}
		};
	});

}());
